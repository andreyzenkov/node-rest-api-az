var express  = require('express'),
    jwt = require('jwt-simple'),
    secret = 'andrey zenkov secret',
    path  = require('path'),
    passport  = require('passport'),
    bodyParser = require('body-parser'),
    config = require('./config/config');
    UserModel = require('./libs/mongoose').UserModel,
    spawn  = require('child_process').spawn,
    BearerStrategy = require('passport-http-bearer').Strategy,
    UserModel = require('./libs/mongoose').UserModel,
    AccessTokenModel = require('./libs/mongoose').AccessTokenModel,
    cors = require('cors');

function validatePhone(phone) {
    var regex = /\d{11}/;

    if (regex.test(phone)) {
        return true;
    } else {
        return false;
    }
}

function validateEmail(email) {
    var regex = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;

    if (regex.test(email)) {
        return true;
    } else {
        return false;
    }
}

var app = express();

passport.use(new BearerStrategy(
    function(accessToken, done) {
        AccessTokenModel.findOne({ token: accessToken }, function(err, token) {
            if (err) { return done(err); }
            if (!token) { return done(null, false); }

            if( Math.round((Date.now()-token.created)/1000) > config.security.tokenLife ) {
                return done(null, false, { message: 'Token expired' });
            } else {
                token.created =  Date.now();
                token.save();
            }

            UserModel.findById(token.user_id, function(err, user) {
                if (err) { return done(err); }
                if (!user) { return done(null, false, { message: 'Unknown user' }); }

                var info = { scope: '*' }
                done(null, user, info);
            });
        });
    }
));

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(passport.initialize());

app.post('/signup', function(req, res){
    var id = req.body.id,
        password = req.body.password;

    if(validateEmail(id) || validatePhone(id)) {
        var user = new UserModel({ 
            _id: id, 
            id_type: validateEmail(id) ? 'email' : 'phone', 
            password: password 
        });

        user.save(function(err, user) {
            if(err) { 
                res.send({error: err});
            } else {
                var t = jwt.encode(user, secret);
                
                var token = new AccessTokenModel({
                    user_id: id,
                    token: t,
                });

                token.save(function(err, user) { 
                    if(err) {
                        console.error(err);
                    } else {
                        res.send({token: t});
                    }
                });    
            }
        });
    } else {
        res.send({error: 'id must be phone number or email adress'});
    }
});

app.post('/signin', function(req, res){
    var id = req.body.id,
        password = req.body.password;

    UserModel.findById(id, function (err, user) {
        if(err) {
            res.send({error: err});
        } else {
            if(!user) {
                res.send({error: 'user not exist'});
            }
            if(user.checkPassword(password)){
                var t = jwt.encode(user, secret);
            
                var token = new AccessTokenModel({
                    user_id: id,
                    token: t,
                });

                token.save(function(err, user) { 
                    if(err) {
                        res.send({error: err});
                    } else {
                        res.send({token: t});
                    }
                });
            } else {
                res.send({error: 'check password'});
            }   
        }
    });

});

app.get('/info', passport.authenticate('bearer', { session: false }), function(req, res) {
    res.send({id: req.user._id, type: req.user.id_type});
});

app.get('/latency', passport.authenticate('bearer', { session: false }), function (req, res) {
    ping = spawn('ping', ['google.com']);

    ping.stdout.on('data', function (data) {
        ping.kill();
        res.send(data.toString());
    });
});

app.get('/logout', passport.authenticate('bearer', { session: false }), function (req, res) {
    if(req.param('all') && (req.param('all') === 'false' || req.param('all') === 'true') ) {
        var token = req.query.access_token;
        if(req.param('all') === 'true') { 
            AccessTokenModel.remove({user_id: req.user._id}, function(err, docs){
                if (err) { 
                    res.send({error: err});
                } else {
                    res.send({docs: docs});
                }
            });
        } else {
            AccessTokenModel.remove({token: token}, function(err, docs){
                if (err) { 
                    res.send({error: err});
                } else {
                    res.send({docs: docs});
                }
            });
        }
    } else {
        res.send({error: 'all parameter is required'});
    }
});

app.listen(config.port, function(){
    console.log('Express server listening on port ' + config.port);
});